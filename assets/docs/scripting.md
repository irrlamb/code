<!---
grep "###" funcs.txt | gawk 'BEGIN{FS="### "}{f=tolower($2); gsub(/[^a-zA-Z ]/, "", f); gsub(/ /, "-", f); print "[" $2 "](#" f ")  "}'
-->

### Functions
- [Camera.SetYaw(Degrees)](#camerasetyawdegrees)
- [Camera.SetPitch(Degrees)](#camerasetpitchdegrees)
- [Object.GetPointer(ObjectName)](#objectgetpointerobjectname)
- [Object.GetName(Object)](#objectgetnameobject)
- [Object.GetTemplate(Object)](#objectgettemplateobject)
- [Object.SetPosition(Object, X, Y, Z)](#objectsetpositionobject-x-y-z)
- [Object.SetRotation(Object, Pitch, Yaw, Roll)](#objectsetrotationobject-pitch-yaw-roll)
- [Object.GetPosition(Object)](#objectgetpositionobject)
- [Object.SetScale(Object, ScaleX, ScaleY, ScaleZ)](#objectsetscaleobject-scalex-scaley-scalez)
- [Object.SetShape(Object, ShapeX, ShapeY, ShapeZ)](#objectsetshapeobject-shapex-shapey-shapez)
- [Object.SetLinearVelocity(Object, VelocityX, VelocityY, VelocityZ)](#objectsetlinearvelocityobject-velocityx-velocityy-velocityz)
- [Object.SetAngularVelocity(Object, VelocityX, VelocityY, VelocityZ)](#objectsetangularvelocityobject-velocityx-velocityy-velocityz)
- [Object.Stop(Object)](#objectstopobject)
- [Object.SetLifetime(Object, Seconds)](#objectsetlifetimeobject-seconds)
- [Object.SetSleep(Object, State)](#objectsetsleepobject-state)
- [Object.Delete(Object)](#objectdeleteobject)
- [Orb.Deactivate(OrbObject, OnDeactivateFunction, DeactivateTime)](#orbdeactivateorbobject-ondeactivatefunction-deactivatetime)
- [Orb.GetState(OrbObject)](#orbgetstateorbobject)
- [Timer.Callback(Function, Time)](#timercallbackfunction-time)
- [Timer.Stamp()](#timerstamp)
- [Level.Win(HideNextLevel)](#levelwinhidenextlevel)
- [Level.Lose(Message)](#levellosemessage)
- [Level.Change(CampaignIndex, LevelIndex)](#levelchangecampaignindex-levelindex)
- [Level.GetTemplate(TemplateName)](#levelgettemplatetemplatename)
- [Level.CreateObject(ObjectName, Template, PositionX, PositionY, PositionZ, RotationX, RotationY, RotationZ)](#levelcreateobjectobjectname-template-positionx-positiony-positionz-rotationx-rotationy-rotationz)
- [Level.CreateConstraint(ConstraintName, ConstraintTemplate, ObjectA, ObjectB)](#levelcreateconstraintconstraintname-constrainttemplate-objecta-objectb)
- [GUI.Fade(Speed)](#guifadespeed)
- [GUI.Text(Text, Time, Tutorial)](#guitexttext-time-tutorial)
- [Audio.Play(SoundFile, PositionX, PositionY, PositionZ, Looping, MinGain, MaxGain, ReferenceDistance, RollOff)](#audioplaysoundfile-positionx-positiony-positionz-looping-mingain-maxgain-referencedistance-rolloff)
- [Audio.Stop(Sound)](#audiostopsound)
- [Random.Seed(Seed)](#randomseedseed)
- [Random.GetFloat(Min, Max)](#randomgetfloatmin-max)
- [Random.GetInt(Min, Max)](#randomgetintmin-max)

### Camera.SetYaw(Degrees)

Sets the yaw for the camera. Degrees is from 0 to 360.

```
-- Set camera to 90 degrees so it faces the right
Camera.SetYaw(90)
```

### Camera.SetPitch(Degrees)

Sets the pitch for the camera. Degrees is from -89 to 89.

```
-- Set camera to 35 degrees so it faces downwards
Camera.SetPitch(35)
```

### Object.GetPointer(ObjectName)

Returns a pointer to an object given a name.

```
-- Get orb pointer
Orb = Object.GetPointer("orb")
```

### Object.GetName(Object)

Returns the name of an object given a pointer.

```
-- Get object name
ObjectName = Object.GetName(Object)
```

### Object.GetTemplate(Object)

Returns the template of an object given a pointer.

```
-- Get template
OrbTemplate = Object.GetTemplate(Object)
```

### Object.SetPosition(Object, X, Y, Z)

Sets the position of an object.

```
-- Set position of the player
Object.SetPosition(Player, 0, 5, 2)
```

### Object.SetRotation(Object, Pitch, Yaw, Roll)

Sets the rotation of an object in euler angles.

```
-- Rotation object by 90 degrees
Object.SetRotation(Box, 0, 90, 0)
```

### Object.GetPosition(Object)

Gets the position of an object.

```
X, Y, Z = Object.GetPosition(Player)
```

### Object.SetScale(Object, ScaleX, ScaleY, ScaleZ)

Sets the mesh scale of an object

```
-- Double the size
Object.SetScale(Object, 2, 2, 2)
```

### Object.SetShape(Object, ShapeX, ShapeY, ShapeZ)

Sets the shape of an object

```
-- Increase shape size
Object.SetShape(Object, 2, 2, 2)
```

### Object.SetLinearVelocity(Object, VelocityX, VelocityY, VelocityZ)

Sets the linear velocity of an object.

```
-- Set the object moving upwards
Object.SetLinearVelocity(Object, 0, 10, 0)
```

### Object.SetAngularVelocity(Object, VelocityX, VelocityY, VelocityZ)

Sets the angular velocity of an object.

```
-- Set the cylinder to rotate about the z axis
Object.SetAngularVelocity(Cylinder, 0, 0, 2)
```

### Object.Stop(Object)

Stops the linear and angular motion of an object.

```
-- Stop the player from moving
Object.Stop(Player)
```

### Object.SetLifetime(Object, Seconds)

Stops the lifetime of an object in seconds. After the elapsed time, the object is deleted.

```
-- Delete the object in 5 seconds
Object.SetLifetime(Object, 5)
```

### Object.SetSleep(Object, State)

Sets the sleep state of an object.

```
-- Wake an object
Object.SetSleep(Object, 0)
```

### Object.Delete(Object)

Deletes an object.

```
-- Delete the object
Object.Delete(Object)
```

### Orb.Deactivate(OrbObject, OnDeactivateFunction, DeactivateTime)

Deactivates an orb. DeactivateTime is specified in seconds, and calls OnDeactivateFunction after that time.

```
-- Collision callback for an orb
function OnHitOrb(MainObject, OtherObject)
        if OtherObject == Player then
                Orb.Deactivate(MainObject, "OnOrbDeactivate", 2)
        end
end
```

### Orb.GetState(OrbObject)

Gets the activation state of an orb.

* 0 = ACTIVE
* 1 = DEACTIVATING
* 2 = DEACTIVATED

```
State = Orb.GetState(Orb)
```

### Timer.Callback(Function, Time)

After "Time" seconds, "Function" is called.

```
-- Call a function after 20 seconds
Timer.DelayedFunction("ShowMoreText", 20)
```

### Timer.Stamp()

Returns the number of seconds since the level was started.

```
-- Get a timestamp
Seconds = Timer.Stamp()
```

### Level.Win(HideNextLevel)

Wins the level. Set HideNextLevel = 1 to disable "Next Level" button. Defaults to 0.

```
-- Called when an orb is deactivated
function OnOrbDeactivate()
        GoalCount = GoalCount - 1
        if GoalCount == 0 then
                Level.Win()
        end
end
```

### Level.Lose(Message)

Loses the level with an optional message.

```
-- Display lose message
function OnHitZone(HitType, Zone, HitObject)
	if HitObject == Player then
		Level.Lose("You drowned!")
		return 1
	end

	return 0
end
```

### Level.Change(CampaignIndex, LevelIndex)

Changes the level.

```
-- Change to the second tutorial level
Level.Change(0, 1)
```

### Level.GetTemplate(TemplateName)

Returns a pointer to a template.

```
-- Get the crate template from the xml file
CrateTemplate = Level.GetTemplate("crate")
```

### Level.CreateObject(ObjectName, Template, PositionX, PositionY, PositionZ, RotationX, RotationY, RotationZ)

Creates an object from a template. RotationX, RotationY, RotationZ are optional.

```
-- Create a new object at Y=5
NewObject = Level.CreateObject("crate", CrateTemplate, 0, 5, 0)
```

### Level.CreateConstraint(ConstraintName, ConstraintTemplate, ObjectA, ObjectB)

Creates a constraint between two objects. If the second object is null it's tied to the world.

```
ConstraintTemplate = Level.GetTemplate("constraint_z")
LogTemplate = Level.GetTemplate("log")
LogObject = Level.CreateObject("log", LogTemplate, 0, 5, 8, 0, 0, 0)
Level.CreateConstraint("constraint", ConstraintTemplate, LogObject, nil)
```

### GUI.Fade(Speed)

Fade the screen.

```
-- Fade out at 5x speed
GUI.Fade(-5)
```

### GUI.Text(Text, Time, Tutorial)

Displays a textbox on the screen for "Time" seconds. Use \n for a new line. Set tutorial = 1 to flag it as a tutorial message.

```
-- Display some text
GUI.Text("Here is some text.\nHere is some more text.", 15)
```

### Audio.Play(SoundFile, PositionX, PositionY, PositionZ, Looping, MinGain, MaxGain, ReferenceDistance, RollOff)

Play a sound. The first 4 arguments are required.

```
-- Play a sound when the zone is hit
function OnHitZone(HitType, Zone, HitObject)
	X, Y, Z = Object.GetPosition(HitObject)
	Sound = Audio.Play("splash.ogg", X, Y, Z, 0, 0.3, 0.7)

	return 0
end
```

### Audio.Stop(Sound)

Stop a sound from playing.

```
Audio.Stop(LoopingSound)
```

### Random.Seed(Seed)

Sets the seed for the random number generator.

```
-- Set seed
Random.Seed(50)
```

### Random.GetFloat(Min, Max)

Returns a random float between Min and Max

```
-- Get a random number from 0.5 to 2.5
X = Random.GetFloat(0.5, 2.5)
```

### Random.GetInt(Min, Max)

Returns a random integer between Min and Max

```
-- Get a random number from 10 to 20
Time = Random.GetInt(10, 20)
```
