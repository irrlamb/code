#!/bin/bash

# parameters
repo_path="$1"
if [ -z "$repo_path" ]; then
	echo "Usage: ./$(basename "$0") repo_path"
	exit 1
fi

# setup
mkdir -p out

# variables
project=irrlamb
version=$(grep 'GAME_VERSION=".*"' -o ../CMakeLists.txt | sed -r "s/GAME_VERSION=\"(.*)\"/\1/")
gitver=$(git log --oneline | wc -l)
base=${project}-${version}r${gitver}
pkg=${base}.flatpak

# make src package
./make_src.sh
cp "out/$base-src.tar.gz" "out/src.tar.gz"

# build package
flatpak-builder --force-clean --repo="$repo_path" --state-dir=flatpak-state flatpak-build flatpak.yml
flatpak build-bundle "$repo_path" "out/$pkg" "io.gitlab.jazztickets.$project"

# clean up
rm -rf flatpak-build flatpak-state "out/src.tar.gz"
