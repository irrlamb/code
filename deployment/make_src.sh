#!/bin/bash

# setup
mkdir -p out

# variables
project=irrlamb
version=$(grep 'GAME_VERSION=".*"' -o ../CMakeLists.txt | sed -r "s/GAME_VERSION=\"(.*)\"/\1/")
gitver=$(git log --oneline | wc -l)
base=${project}-${version}r${gitver}
pkg=${base}-src.tar.gz

# build package
tar --transform "s,^,$base/," -czvf "out/$pkg" -C ../ \
--exclude="$pkg" \
--exclude=move.{sh,bat} \
--exclude=*.swp \
--exclude=.git \
--exclude=working/"$project"* \
--exclude=working/colmesh* \
src/ \
working/ \
deployment/irrlamb{,.desktop,.png,.xml} \
cmake/ \
build.sh \
CMakeLists.txt \
README \
CHANGELOG \
LICENSE

echo -e "\nMade $pkg"
